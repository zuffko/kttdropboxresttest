extern NSString *const GET;
extern NSString *const PUT;
extern NSString *const POST;
extern NSString *const sandbox;

#pragma Dropbox REST login keys
extern NSString *const loginResponseType;
extern NSString *const loginResponseTypeToken;
extern NSString *const loginClientId;
extern NSString *const loginClientIdValue;
extern NSString *const loginRedirectUri;
extern NSString *const loginRedirectUriValue;
extern NSString *const loginBaseUrl;
extern NSString *const loginPath;

#pragma Dropbox REST uploadFile keys
extern NSString *const uploadFileBaseUrl;
extern NSString *const uploadFilePath;
extern NSString *const uploadFileHeaderTokenBearer;
extern NSString *const uploadFileHeaderFieldName;

#pragma Dropbox REST metadata keys
extern NSString *const metadataFileBaseUrl;
extern NSString *const metadataFilePath;
extern NSString *const metadataFileHeaderTokenBearer;
extern NSString *const metadataFileHeaderFieldName;

#pragma Dropbox REST sharedLink keys
extern NSString *const sharedLinkFileBaseUrl;
extern NSString *const sharedLinkFilePath;
extern NSString *const sharedLinkFileHeaderTokenBearer;
extern NSString *const sharedLinkFileHeaderFieldName;

#pragma Allert text
extern NSString *const allertTextLogin;
extern NSString *const allertTextPickFile;
extern NSString *const allertTextUploadFile;
extern NSString *const allertTextUploadFile_SharedLink;

#pragma Action Sheet keys
extern NSString *const actionSheetSelectImageFrom;
extern NSString *const actionSheetCancel;
extern NSString *const actionSheetFromcamera;
extern NSString *const actionSheetFromLibrary;

