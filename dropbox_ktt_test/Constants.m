#import "Constants.h"

NSString *const GET = @"GET";
NSString *const PUT = @"PUT";
NSString *const POST = @"POST";
NSString *const sandbox = @"sandbox";

#pragma Dropbox REST login keys
NSString *const loginResponseType = @"response_type";
NSString *const loginResponseTypeToken = @"token";
NSString *const loginClientId = @"client_id";
NSString *const loginClientIdValue = @"oam82wfiayrk6m5";
NSString *const loginRedirectUri = @"redirect_uri";
NSString *const loginRedirectUriValue = @"db-oam82wfiayrk6m5://";
NSString *const loginBaseUrl = @"https://www.dropbox.com";
NSString *const loginPath = @"/1/oauth2/authorize";

#pragma Dropbox REST uploadFile keys
NSString *const uploadFileBaseUrl = @"https://api-content.dropbox.com";
NSString *const uploadFilePath = @"/1/files_put/sandbox/%@.jpg?param=val";
NSString *const uploadFileHeaderTokenBearer = @"Bearer %@";
NSString *const uploadFileHeaderFieldName = @"Authorization";

#pragma Dropbox REST metadata keys
NSString *const metadataFileBaseUrl = @"https://api.dropbox.com";
NSString *const metadataFilePath = @"/1/metadata/%@%@";
NSString *const metadataFileHeaderTokenBearer = @"Bearer %@";
NSString *const metadataFileHeaderFieldName = @"Authorization";

#pragma Dropbox REST sharedLink keys
NSString *const sharedLinkFileBaseUrl = @"https://api.dropbox.com";
NSString *const sharedLinkFilePath = @"1/shares/%@%@";
NSString *const sharedLinkFileHeaderTokenBearer = @"Bearer %@";
NSString *const sharedLinkFileHeaderFieldName = @"Authorization";

#pragma Allert text
NSString *const allertTextLogin = @"Please Log In";
NSString *const allertTextPickFile = @"Please pick file";
NSString *const allertTextUploadFile = @"Please Upload file";
NSString *const allertTextUploadFile_SharedLink =@"Please press shared link after file uploading" ;

#pragma Action Sheet keys
NSString *const actionSheetSelectImageFrom = @"Select image from";
NSString *const actionSheetCancel = @"Cancel";
NSString *const actionSheetFromcamera = @"From camera";
NSString *const actionSheetFromLibrary = @"From library";
