//
//  Random.h
//  dropbox_ktt_test
//
//  Created by zufa on 20/7/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Random : NSObject
+ (NSString *) genRandStringLength: (int) len;
@end
