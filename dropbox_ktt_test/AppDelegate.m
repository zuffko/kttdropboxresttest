
#import "AppDelegate.h"

@implementation AppDelegate
@synthesize myString;

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if (!url) {  return NO; }    
    NSArray *parts = [[url fragment] componentsSeparatedByString:@"&"];
    for ( int i =0 ; i< [parts count]; i++) {
        if (i == 0) {
            myString= [[parts objectAtIndex:i] substringFromIndex:13];
        }
    }
    return YES;
}


@end
