#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property  (nonatomic, retain) IBOutlet UILabel *urlLabel;

@end
