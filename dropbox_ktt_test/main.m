//
//  main.m
//  dropbox_ktt_test
//
//  Created by zufa on 16/7/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
