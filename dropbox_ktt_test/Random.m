//
//  Random.m
//  dropbox_ktt_test
//
//  Created by zufa on 20/7/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "Random.h"

@implementation Random

+ (NSString *) genRandStringLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    
    return randomString;
}

@end
