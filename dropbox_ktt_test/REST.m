//
//  REST.m
//  dropbox_ktt_test
//
//  Created by zufa on 20/07/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import "REST.h"
#import "AppDelegate.h"

@implementation REST

@synthesize client=_client;
@synthesize request=_request;
@synthesize requestparameters=_requestparameters;
@synthesize operation=_operation;


-(void) login
{
    _requestparameters = [NSDictionary dictionaryWithObjectsAndKeys: loginResponseTypeToken, loginResponseType, loginClientIdValue, loginClientId, loginRedirectUriValue, loginRedirectUri, nil];
    
    _client = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:loginBaseUrl]];
    _client.parameterEncoding = AFJSONParameterEncoding;
    
    _request = [_client requestWithMethod:GET path:loginPath parameters:_requestparameters];
    [[UIApplication sharedApplication] openURL:[_request URL]];

}

-(void) uploadFile:(UIImage *)imageData
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    token = appDelegate.myString;
    
    if (!token) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"allertNotification" object:allertTextLogin];
    } else{
        if (!imageData) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"allertNotification" object:allertTextPickFile];
        }else{
            _client = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:uploadFileBaseUrl]];
            _client.parameterEncoding = AFJSONParameterEncoding;
            
            _request = [_client requestWithMethod:PUT path:[NSString stringWithFormat: uploadFilePath , [Random genRandStringLength:10]]  parameters:nil];
            
            [_request addValue: [NSString stringWithFormat: uploadFileHeaderTokenBearer , token] forHTTPHeaderField:uploadFileHeaderFieldName];
            NSData *Data = UIImageJPEGRepresentation(imageData, 0.5);
            [_request setHTTPBody:Data];
            
            _operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:_request
                                                                                                success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                                 {
                                                     NSLog(@"JSON: %@", [JSON valueForKeyPath:@"root"]);
                                                     root=[JSON valueForKeyPath:@"root"];
                                                     NSLog(@"JSON: %@", [JSON valueForKeyPath:@"path"]);
                                                     path = [JSON valueForKeyPath:@"path"];
                                                 }
                                                                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                                 {
                                                     NSLog(@"%@", error);
                                                 }];
            [_operation start];
        }
    }
}

-(void) getMetadata
{
    if (!root || !path) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"allertNotification" object:allertTextUploadFile];
    }else{
        _client = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:metadataFileBaseUrl]];
        _client.parameterEncoding = AFJSONParameterEncoding;
        
        _request = [_client requestWithMethod:GET path:[NSString stringWithFormat: metadataFilePath , sandbox, path]  parameters:nil];
        
        [_request addValue: [NSString stringWithFormat: metadataFileHeaderTokenBearer , token] forHTTPHeaderField:metadataFileHeaderFieldName];
        _operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:_request
                                                                                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                             {
                                                 NSLog(@"JSON getMetadata : %@", [JSON valueForKeyPath:@"root"]);
                                                 NSLog(@"JSON getMetadata : %@", [JSON valueForKeyPath:@"path"]);
                                             }
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                             {
                                                 NSLog(@"%@", error);
                                             }];
        [_operation start];
    }
}

-(void) sharedLink
{
    if (!root || !path) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"allertNotification" object:allertTextUploadFile];
    }else{
        _client = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:sharedLinkFileBaseUrl]];
        _client.parameterEncoding = AFJSONParameterEncoding;
        
        _request = [_client requestWithMethod:POST path:[NSString stringWithFormat: sharedLinkFilePath , sandbox, path]  parameters:nil];
        
        [_request addValue: [NSString stringWithFormat: sharedLinkFileHeaderTokenBearer , token] forHTTPHeaderField:sharedLinkFileHeaderFieldName];
        
        _operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:_request
                                             
                                                                                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                             {
                                                 NSLog(@"JSON expires : %@", [JSON valueForKeyPath:@"expires"]);
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"showFileUrl" object:[JSON valueForKeyPath:@"url"]];
                                                 
                                             }
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                             {
                                                 NSLog(@"%@", error);
                                             }];
        [_operation start];
    }
}

@end

