#import "ViewController.h"
#import "AFJSONRequestOperation.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "Random.h"
#import "AFHTTPClient.h"
#import "REST.h"

@interface ViewController ()
@end

@implementation ViewController{
    UIImage *pickedImage;
    NSString *fileUrl;
    REST *rest;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(allertWithMessage:)
                                                 name:@"allertNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showFileUrl:)
                                                 name:@"showFileUrl"
                                               object:nil];
    rest = [REST new];
}

- (IBAction) login:(id)sender{
    [rest login];
}

- (IBAction) Uploadfile:(id)sender{
    [rest uploadFile:pickedImage];
}

- (IBAction) getMetadata:(id)sender{
    [rest getMetadata];
}

- (IBAction) sharedLink:(id)sender{
    [rest sharedLink];
}

- (IBAction)openUrl:(id)sender{
    if (!fileUrl) {
        [self showAllertWithMessage:allertTextUploadFile_SharedLink];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fileUrl]];
    }
}

- (void) allertWithMessage:(NSNotification*)notification {
    [self showAllertWithMessage:[notification object]];
}

- (void) showAllertWithMessage:(NSString *)string {
    [[[UIAlertView alloc] initWithTitle:@"Warning" message:[NSString stringWithFormat: @"%@" , string] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void) showFileUrl:(NSNotification*)notification{
    fileUrl=[notification object];
    _urlLabel.text=fileUrl;
}

- (IBAction) addImage:(id)sender {
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:actionSheetSelectImageFrom
                                                         delegate:self cancelButtonTitle:actionSheetCancel
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:actionSheetFromcamera, actionSheetFromLibrary, nil];
    [self.view endEditing:YES];
    [action showInView:self.view];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if( buttonIndex == 0 ) {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *pickerView =[[UIImagePickerController alloc]init];
            pickerView.allowsEditing = YES;
            pickerView.delegate = self;
            pickerView.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:pickerView animated:YES completion:nil];
        }
        
    }
    else if( buttonIndex == 1 )
    {
        
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            pickerController.allowsEditing = YES;
            pickerController.delegate = self;
            
            [self presentViewController:pickerController animated:YES completion:NULL];
        }
    }
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    if (image == nil)
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    pickedImage=image;
}

@end
