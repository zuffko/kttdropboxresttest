//
//  REST.h
//  dropbox_ktt_test
//
//  Created by zufa on 20/7/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Random.h"
#import "AFHTTPClient.h"
#import "Constants.h"
#import "AFJSONRequestOperation.h"

@interface REST : NSObject {
    NSString *token;
    NSString *root;
    NSString *path;
}

@property (nonatomic , retain) AFHTTPClient *client;
@property (nonatomic , retain) NSMutableURLRequest *request;
@property (nonatomic , retain) NSDictionary *requestparameters;
@property (nonatomic , retain) AFHTTPRequestOperation *operation;

-(void) login;
-(void) uploadFile:(UIImage *)imageData;
-(void) getMetadata;
-(void) sharedLink;


@end
